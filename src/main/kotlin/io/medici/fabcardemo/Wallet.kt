package io.medici.fabcardemo

import org.hyperledger.fabric.gateway.Wallet
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Component
import java.io.IOException
import java.io.StringReader
import java.io.StringWriter
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.security.PrivateKey
import java.util.*
import javax.json.Json
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * Support for JPA backed wallet
 */
@Entity
@Table(name = "wallet")
data class WalletEntity(
  @Id
  @Column(name = "label")
  var label: String,

  @Column(name = "json")
  var json: String,

  @Column(name = "private_key")
  var privateKey: String
)

interface WalletRepository: JpaRepository<WalletEntity, String>

@Component
class JpaWallet(val walletRepository: WalletRepository): Wallet {
  override fun getAllLabels(): MutableSet<String> = walletRepository
      .findAll()
      .map { it.label }
      .toMutableSet()

  override fun put(label: String, identity: Wallet.Identity) {
    walletRepository.save(WalletEntity(
      label = label,
      json = toJson(label, identity),
      privateKey = toPrivateKey(identity.privateKey)
    ))
  }

  override fun remove(label: String) {
    walletRepository
      .findById(label)
      .ifPresent {
        walletRepository.delete(it)
      }
  }

  override fun get(label: String): Wallet.Identity? =
    walletRepository.findById(label)
      .map { fromJson(it.json, it.privateKey) }
      .get()

  override fun exists(label: String): Boolean = walletRepository.existsById(label)

  private fun fromJson(json: String, privateKey: String): Wallet.Identity? {
    Json.createReader(StringReader(json)).use { reader ->
      val idObject = reader.readObject()
      val mspId = idObject.getString("mspid")
      val enrollment = idObject.getJsonObject("enrollment")
      val certificate = enrollment.getJsonObject("identity").getString("certificate")
      return Wallet.Identity.createIdentity(mspId, StringReader(certificate), StringReader(privateKey))
    }
  }

  private fun toJson(name: String, identity: Wallet.Identity): String {
    val idObject = Json.createObjectBuilder()
      .add("name", name)
      .add("type", "X509")
      .add("mspid", identity.mspId)
      .add("enrollment", Json.createObjectBuilder()
        .add("signingIdentity", name)
        .add("identity", Json.createObjectBuilder()
          .add("certificate", identity.certificate)))
      .build()
    val writer = StringWriter()
    Json.createWriter(writer).use { jw -> jw.writeObject(idObject) }
    return writer.toString()
  }

  private fun toPrivateKey(key: PrivateKey): String =
    StringWriter().use { writer ->
      writer.append("-----BEGIN PRIVATE KEY-----\n")
      val base64 = Base64.getEncoder().encodeToString(key.encoded)
      Arrays.stream(base64.split("(?<=\\G.{64})")
        .toTypedArray())
        .forEachOrdered {
          line: String -> writer.append("$line\n")
        }
      writer.append("-----END PRIVATE KEY-----\n")
    }.toString()

}