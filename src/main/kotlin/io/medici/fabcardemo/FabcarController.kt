package io.medici.fabcardemo

import org.hyperledger.fabric.gateway.Gateway
import org.hyperledger.fabric.gateway.Wallet
import org.hyperledger.fabric.sdk.Enrollment
import org.hyperledger.fabric.sdk.User
import org.hyperledger.fabric_ca.sdk.EnrollmentRequest
import org.hyperledger.fabric_ca.sdk.HFCAClient
import org.hyperledger.fabric_ca.sdk.RegistrationRequest
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.nio.file.Paths
import java.security.PrivateKey

/**
 * Rest Controller for all operations to interact with the fabcar smart contract and user management
 */
@RestController
class FabcarController(
  val wallet: Wallet,
  val caClient: HFCAClient,
  val fabcarProperties: FabcarProperties
) {

  companion object {
    init {
      System.setProperty("org.hyperledger.fabric.sdk.service_discovery.as_localhost", "true")
    }
  }

  @PostMapping("/admins")
  fun enrollAdmin(): ResponseEntity<Any> {
    val username = fabcarProperties.adminUsername
    if (wallet.exists(username)) {
      return ResponseEntity.badRequest().body("Admin already enrolled")
    }
    val enrollmentRequest = EnrollmentRequest()
    enrollmentRequest.addHost(fabcarProperties.enrollmentRequestHost)
    enrollmentRequest.profile = fabcarProperties.enrollmentRequestProfile
    val enrollment = caClient.enroll(username, fabcarProperties.adminPassword, enrollmentRequest)
    val user = Wallet.Identity.createIdentity(fabcarProperties.mspId, enrollment.cert, enrollment.key)
    wallet.put(username, user)
    return ResponseEntity.ok("Admin enrolled")
  }

  @PostMapping("/users")
  fun registerUser(@RequestBody registerUserRequest: RegisterUserRequest): ResponseEntity<Any> {
    val username = registerUserRequest.username
    val userAffiliation = registerUserRequest.affiliation
    if (wallet.exists(username)) {
      return ResponseEntity.badRequest().body("User: $username already exists")
    }
    val adminUsername = fabcarProperties.adminUsername
    if (!wallet.exists(adminUsername)) {
      return ResponseEntity.badRequest().body("Admin user needs to be enrolled and added to the wallet first")
    }
    val adminIdentity = wallet.get(adminUsername)
    val admin = createAdminUser(adminIdentity, userAffiliation)
    val registrationRequest = RegistrationRequest(username).apply {
      affiliation = userAffiliation
      enrollmentID = username
    }
    val enrollmentSecret = caClient.register(registrationRequest, admin)
    val enrollment = caClient.enroll(username, enrollmentSecret)
    val user = Wallet.Identity.createIdentity(fabcarProperties.mspId, enrollment.cert, enrollment.key)
    wallet.put(username, user)
    return ResponseEntity.ok(registerUserRequest)
  }

  @PostMapping("/queryAllCars", produces = ["application/json"])
  fun queryAllCars(@RequestBody request: QueryAllCarsRequest): ResponseEntity<Any> {
    val result = Gateway
      .createBuilder()
      .identity(wallet, request.asUser)
      .networkConfig(Paths.get(fabcarProperties.networkConfigPath))
      .discovery(true)
      .connect()
      .use {
        val network = it.getNetwork("mychannel")
        val contract = network.getContract("fabcar")
        contract.evaluateTransaction("queryAllCars")
      }
    return ResponseEntity.ok(String(result))
  }

  @PostMapping("/createCar")
  fun createCar(@RequestBody request: CreateCarRequest): ResponseEntity<Any> {
    val result = Gateway
      .createBuilder()
      .identity(wallet, request.asUser)
      .networkConfig(Paths.get(fabcarProperties.networkConfigPath))
      .discovery(true)
      .connect()
      .use {
        val network = it.getNetwork("mychannel")
        val contract = network.getContract("fabcar")
        contract.submitTransaction(
          "createCar",
          request.carId,
          request.carMake,
          request.carModel,
          request.carColor,
          request.asUser
        )
      }
    return ResponseEntity.ok(String(result))
  }

  @PostMapping("/queryCar", produces = ["application/json"])
  fun queryCar(@RequestBody request: QueryCarRequest): ResponseEntity<Any> {
    val result = Gateway
      .createBuilder()
      .identity(wallet, request.asUser)
      .networkConfig(Paths.get(fabcarProperties.networkConfigPath))
      .discovery(true)
      .connect()
      .use {
        val network = it.getNetwork("mychannel")
        val contract = network.getContract("fabcar")
        contract.evaluateTransaction(
          "queryCar",
          request.carId
        )
      }
    return ResponseEntity.ok(String(result))
  }

  @PostMapping("/changeCarOwner")
  fun changeCarOwner(@RequestBody request: ChangeCarOwnerRequest): ResponseEntity<Any> {
    val result = Gateway
      .createBuilder()
      .identity(wallet, request.asUser)
      .networkConfig(Paths.get(fabcarProperties.networkConfigPath))
      .discovery(true)
      .connect()
      .use {
        val network = it.getNetwork("mychannel")
        val contract = network.getContract("fabcar")
        contract.submitTransaction(
          "changeCarOwner",
          request.carId,
          request.asUser
        )
      }
    return ResponseEntity.ok(String(result))
  }

  private fun createAdminUser(adminIdentity: Wallet.Identity, affiliation: String): User {
    return object : User {
      override fun getEnrollment(): Enrollment = object : Enrollment {
        override fun getKey(): PrivateKey = adminIdentity.privateKey
        override fun getCert(): String = adminIdentity.certificate
      }

      override fun getName(): String = fabcarProperties.adminUsername
      override fun getRoles(): MutableSet<String> = emptySet<String>().toMutableSet()
      override fun getAffiliation(): String = affiliation
      override fun getAccount(): String? = null
      override fun getMspId(): String = fabcarProperties.mspId
    }
  }

}