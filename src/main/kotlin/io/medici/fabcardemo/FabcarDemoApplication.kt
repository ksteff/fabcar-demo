package io.medici.fabcardemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

/**
 * Main entrypoint for Spring Boot Application
 */
@EnableConfigurationProperties(FabcarProperties::class)
@SpringBootApplication
class FabcarDemoApplication

fun main(args: Array<String>) {
  runApplication<FabcarDemoApplication>(*args)
}