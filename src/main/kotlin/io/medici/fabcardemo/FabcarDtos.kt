package io.medici.fabcardemo

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Data transfer objects for requests/responses
 */
data class RegisterUserRequest(
  @JsonProperty("affiliation")
  val affiliation: String,

  @JsonProperty("username")
  val username: String
)

data class QueryAllCarsRequest(
  @JsonProperty("as_user")
  val asUser: String
)

data class QueryCarRequest(
  @JsonProperty("as_user")
  val asUser: String,

  @JsonProperty("car_id")
  val carId: String
)

data class CreateCarRequest(
  @JsonProperty("as_user")
  val asUser: String,

  @JsonProperty("car_id")
  val carId: String,

  @JsonProperty("car_make")
  val carMake: String,

  @JsonProperty("car_model")
  val carModel: String,

  @JsonProperty("car_color")
  val carColor: String

)

data class ChangeCarOwnerRequest(
  @JsonProperty("as_user")
  val asUser: String,

  @JsonProperty("car_id")
  val carId: String
)