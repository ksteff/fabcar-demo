package io.medici.fabcardemo

import org.hyperledger.fabric.sdk.security.CryptoSuiteFactory
import org.hyperledger.fabric_ca.sdk.HFCAClient
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.*

/**
 * Spring configuration for Hyperledger components
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "fabcar")
data class FabcarProperties(
  val adminUsername: String,
  val adminPassword: String,
  val allowAllHostNames: Boolean,
  val caBaseUrl: String,
  val caCertPath: String,
  val enrollmentRequestHost: String,
  val enrollmentRequestProfile: String,
  val mspId: String,
  val networkConfigPath: String
)

@Configuration
class HyperledgerConfig(val fabcarProperties: FabcarProperties) {

  @Bean
  fun caClient(): HFCAClient {
    val props = Properties()
    props["pemFile"] = fabcarProperties.caCertPath
    props["allowAllHostNames"] = fabcarProperties.allowAllHostNames.toString()
    val caClient = HFCAClient.createNewInstance(fabcarProperties.caBaseUrl, props)
    val cryptoSuite = CryptoSuiteFactory.getDefault().cryptoSuite
    caClient.cryptoSuite = cryptoSuite
    return caClient
  }

}
