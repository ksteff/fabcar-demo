# fabcar-demo #

A sample Spring Boot webservice that interacts with they Hyperledger Fabric
fabcar sample smart contracts.  It includes a Wallet implementation that stores
credentials in a PostgreSQL database which uses JPA for data access.

## Setup Hyperledger Fabric ##

This application is not fully self-contained and will require you to run the
Hyperledger Fabric sample project called fabcar in order to setup your Hyperledger
Fabric network.

Run these commands in your terminal:

```
git clone https://github.com/hyperledger/fabric-samples.git
git clone git@bitbucket.org:ksteff/fabcar-demo.git
cd fabric-samples/first-network
./byfn.sh up
./byfn.sh down
cd ../fabcar
./startFabric.sh
cd ..
cp first-network/crypto-config/peerOrganizations/org1.example.com/ca/ca.org1.example.com-cert.pem ../fabcar-demo/src/main/jib/ca.org1.example.com-cert.pem
cp first-network/connection-org1.yaml ../fabcar-demo/src/main/jib/connection-org1.yaml
```

## Troubleshooting Setup ##

If you run into issues with starting up the local network see the following links:
 
* [Build your first network](https://hyperledger-fabric.readthedocs.io/en/release-1.4/build_network.html)
* [Write your first application](https://hyperledger-fabric.readthedocs.io/en/release-1.4/write_first_app.html)

## Edit Network Configuration ##

Then you'll need to edit `fabcar-demo/src/main/jib/connection-org1.yaml` to point to the host names
used in Docker Compose.  Change the following:

Line 20 from:

```
    url: grpcs://localhost:7051
```

to:

```
    url: grpcs://peer0.org1.example.com:7051
```

Line 43 from:

```
    url: grpcs://localhost:8051
```

to:

```
    url: grpcs://peer1.org1.example.com:8051
```

Line 67 from:

```
    url: https://localhost:7054
```

to:

```
    url: https://ca0:7054
```
## Build and Run fabcar-demo ##

Next you'll need to compile a Docker image for fabcar-demo using gradle.  Run the
following commands in your terminal:

```
cd ../fabcar-demo
./gradlew jibDockerBuild
docker-compose up -d
```

## Verify with Postman ##

There is a postman collection included in the fabcar-demo project which you
can import into postman.  There are the following scenarios that you can test:

* Enroll Admin
* Register User
* Query All Cars
* Create Car
* Query Car
* Change Car Owner

Run through each request and make sure it returns without error.  Then adjust
the request bodies to register another user and try using that user id in other
requests as well.  Also try using a non-existent user id and see the errors
that come back.

## Verify data with psql ##

Install postgresql with:

```
brew install postgresql
``` 

and connect with:

```
psql -h localhost -U fabcar -d fabcar
```

using password `fabcar`

Verify the database by running:

```
fabcar=# \d
fabcar=# select * from wallet;
```

To see the wallet data contained in the PostgreSQL database.

## Additional Troubleshooting ##

If you take the Hyperledger Fabric network down, you'll need to copy new
configuration files and edit them again or you will run into authentication errors.

Copy the following files again from the `fabric-samples` home directory:

```
cp first-network/crypto-config/peerOrganizations/org1.example.com/ca/ca.org1.example.com-cert.pem ../fabcar-demo/src/main/jib/ca.org1.example.com-cert.pem
cp first-network/connection-org1.yaml ../fabcar-demo/src/main/jib/connection-org1.yaml
```

And edit them as mentioned above.

Also, if you rebuild the Hyperledger Fabric network, you'll also need to wipe out
your wallet data from the PostgreSQL database.  While connected with `psql` run:

```
fabcar=# delete from wallet;
```

And re-enroll an admin user to start out fresh.